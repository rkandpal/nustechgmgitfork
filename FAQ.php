<?php 
	
	require_once '_inc.php';
	require_once 'functions.php';
	$head_title = array();
	$head_title[] = 'FAQ';
	require_once '_header.php';
	
?>

<div id="basic-content">

	<h2>Frequently Asked Questions</h2>
	<p><strong>How do I shop with grocerymate?</strong></p>

	<ul>
    	<li>Shop – choose your favourite products and start filling your basket. Shop from your Favourites, browse the Aisles, or use Search to find specific items.</li>
		<li>Save – save money by switching stores or swapping products for cheaper alternatives. To find the cheapest shop simply click on the “Select a supermarket” drop down box on your shopping list page. </li>
		<li>All categories available – You can do a Health & Beauty shop in addition to a Groceries and Liquor shop.</li>
		<li>Better product search – like all the good search engines, if you type in a product into our search bar it will provide predictions of what you are looking for, helping you quickly select your desired item.</li>
		<li>Better products display – we have fewer products displayed on each shelf to make it much easier to see products and choose the right ones.</li>
		<li>Saved Shopping Lists – where you can see your previous orders, savings and use as a base for new lists.</li>	
    </ul>

	<p><strong>Do you have any plans to add any more retailers in the future?</strong></p>
	<p>Yes. We are constantly looking for other retailers to add to our site. We want to give you the maximum choice possible, and the very best value.</p>

	<p><strong>Why should I use grocerymate instead of the supermarkets' own websites?</strong></p>
	<p>With grocerymate you get all the benefits of shopping at your favourite supermarket plus:</p>
	<ul>
    	<li>Compare and save – With grocerymate you can compare the price of any item or the price of your entire basket between retailer stores.</li>
		<li>Impartial, free advice – grocerymate is completely impartial and is free to use. Our only aim is to help customers save as much time and money as possible on their grocery shopping.</li>
	</ul>
    
	<p><strong>How do I find more information about a product?</strong></p>
	<p>Click on the product image or name to see the item's details, including a larger image. This product details page includes ingredients, full nutritional information, allergies info and more. Where details are missing, we are in the process of adding them.</p>

	<p><strong>How can I save money?</strong></p>
	<ul>
    	<li>Swap & Save – You can save money by swapping products for cheaper alternatives.</li>
		<li>Switch Store – We keep track of the price of your basket in all stores while you shop. If another store offers a better price for your entire basket, you can immediately transfer all the contents of your basket to that store. supermarket drop down list on the shopping list page.</li>
		<li>Split List – If you prefer to shop in-store, split your shopping list between two stores to get the best deals in both. To do so, start shopping from one store and when you're ready- click on the supermarket drop down list on the shopping list page. Click "Print Shopping List" and choose whether to print your list for your chosen store or to split it between two stores.</li>
	</ul>
    <p><strong>How do I switch stores?</strong></p>
	<p>By clicking on the supermarket drop down list on the shopping list page.</p>

	<p><strong>Are product prices and special offers up-to-date?</strong></p>
	<p>We update prices and special offers once per week in line with retailers promotions.</p>

	<p><strong>Is grocerymate free to use?</strong></p>
	<p>Yes, grocerymate is completely free. All you have to do is pick a store to start filling your basket.</p>
    <br />
	<p><strong>Technical issues</strong></p>
	<p><strong>What browser versions are supported by grocerymate?</strong></p>
	<p>Currently all latest versions of Internet Explorer, Firefox, Chrome and Safari are supported. If you experience performance issues while using grocerymate, try to update your browser's version or switch to a different one. If this doesn't help, please contact <a href="mailto:support@grocerymate.com.au">support@grocerymate.com.au</a></p>
	<p><strong>What happens if I disable cookies or JavaScript?</strong></p>
	<p>Sorry, grocerymate will not work if cookies or JavaScript are disabled.</p>
	<p><strong>I forgot my password, what should I do?</strong></p>
	<p>Don't worry; we will help you create a new one. Just click 'Sign in' on the menu bar at the top section of any page. When the pop up opens, click the 'Forgotten your password?' link (just above the 'Sign in' button). Then, enter your email address and click the 'Send' button. A link to create your new password will be emailed to you – and when you click on this link, you will be taken to a page which will allow you to set a new password.</p>
	<p><strong>How do I change details in my account?</strong></p>
	<p>To change any of your account details, just click on your username, at the top right corner of the page. Under 'My Account' you will be able to delete and replace any information you choose, including your personal details, your email preferences and stores login details. Please note that in order to change your email address and/or password, you will need to enter your current grocerymate password.</p>
	<p><strong>How do I unsubscribe from grocerymate emails?</strong></p>
	<p>Click on your username, at the top right corner of the page and choose 'My Account'. Choose 'Email Preferences' and untick the box. You can also click on the unsubscribe link at the bottom of the emails you receive from grocerymate.</p>
	<p><strong>How do I prevent grocerymate emails from going to my junk/spam folder?</strong></p>
	<p>Help ensure that your grocerymate emails are always delivered to your inbox. Log in to your email now and add grocerymate to your address book, contacts or "Safe Senders" list. If you need any assistance in doing that, please contact <a href="mailto:support@grocerymate.com.au">support@grocerymate.com.au</a></p>


</div><!-- basic-content -->

<?php

	require_once '_footer.php';
	
?>