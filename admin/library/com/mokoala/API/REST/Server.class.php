<?php

class MK_API_REST_Server
{

	protected $parameters;
	protected $method;
	protected $status;

	protected $response = array(
		'status' => 'ok',
		'status_code' => '200',
		'status_message' => '',
		'body' => array(),
	);

	public function processRequest( $method, $parameters )
	{
		if( !empty($parameters['module']) )
		{
			if( $method == 'post' )
			{
				
			}
			else
			{
				try
				{
					$module = MK_RecordModuleManager::getFromType($parameters['module']);
					unset( $parameters['module'] );

					if( $id = $parameters['id'] )
					{
						$record = MK_RecordManager::getFromId( $module->getId(),  $id );						
						
						$this->response['body'] = $record->toArray();

					}
					else
					{
					
						$records = array();
						$paginator = new MK_Paginator();
						$paginator
							->setPage( !empty($parameters['page']) ? $parameters['page'] : 1 )
							->setPerPage( !empty($parameters['per_page']) ? $parameters['per_page'] : 10 );
	
						unset( $parameters['page'], $parameters['per_page'] );

						$search_parameters = array();
						foreach( $parameters as $parameter_key => $parameter_value )
						{
							$search_parameters[] = array('field' => $parameter_key, 'value' => $parameter_value);
						}
						
						if( $search_parameters )
						{
							$module_records = $module->searchRecords( $search_parameters, $paginator );
						}
						else
						{
							$module_records = $module->getRecords( $paginator );
						}

						foreach( $module_records as $record )
						{
							$records[] = $record->toArray();
						}
						
						$this->response['body'] = array(
							'records' => $records,
							'page' => $paginator->getPage(),
							'per_page' => $paginator->getPerPage(),
							'total_pages' => $paginator->getTotalPages(),
							'total_records' => $paginator->getTotalRecords(),
						);
						
					}

				}
				catch(Exception $e)
				{
					$this->response['status_code'] = '400';
				}
			}
		}
		else
		{
			$this->response['status'] = 'error';
			$this->response['status_code'] = '400';
		}
		
		$this->response['status_message'] = MK_Request::getStatusCode( $this->response['status_code'] );

		return $this->response;
		
	}

}

?>