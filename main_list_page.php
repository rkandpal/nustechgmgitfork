<?php
	
	require_once('functions.php');
	
	require_once('config.php');
	
	require_once('_inc.php');
	
	StartSession();
	
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	$current_list = (isset($_SESSION['list_id']) ? $_SESSION['list_id'] : get_last_id());
	if(isset($_GET['list_archive']) && $_GET['list_archive'] != '')
	{
		$list_archive = intval($_GET['list_archive']);
	}
	//echo $list_archive;
	if($list_archive):
		$query = "SELECT shopping_lists_products.shopping_list_product_sale_id, shopping_lists_products.shopping_list_product_count, shopping_lists_products.price, shopping_lists_products.special_price, products.name, supermarkets.name
				FROM products, product_sales, shopping_lists_products, supermarkets 
				WHERE shopping_lists_products.shopping_list_id = $list_archive AND 
					  product_sales.id = shopping_lists_products.shopping_list_product_sale_id AND
					  products.id = shopping_lists_products.shopping_list_product_id AND
					  shopping_lists_products.supermarket_id = supermarkets.id";
		$list_details = "SELECT name, date FROM shopping_lists WHERE id = $list_archive;";
	else:
		$query = "SELECT shopping_lists_products.shopping_list_product_sale_id, shopping_lists_products.shopping_list_product_count, 
						 product_sales.price, product_sales.special_price,
						 products.name,
						 supermarkets.name
				  FROM products, product_sales, shopping_lists_products, supermarkets
				  WHERE shopping_list_id = $current_list AND
						product_sales.id = shopping_lists_products.shopping_list_product_sale_id AND
				  		products.id = shopping_lists_products.shopping_list_product_id AND
						supermarkets.id = product_sales.supermarket_id";
		$query .= (isset($_GET['supermarket']) && $_GET['supermarket'] != '')? " AND supermarkets.name = '".$_GET['supermarket']."'" : "";
		$query .= " ORDER BY supermarkets.name ASC";
		$list_details = "SELECT name, date FROM shopping_lists WHERE id = $current_list;";
	endif;
		
	$results = $db_link->prepare($query);
	
	$results->bind_result($id, $product_count, $price, $special, $product_name, $supermarket);
	$results->execute();
	$results->store_result();
	
	$list_details_result = $db_link->prepare($list_details);
	$list_details_result->bind_result($list_name, $list_date);
	$list_details_result->execute();
	$list_details_result->store_result();
	
	$supermarkets = $db_link->prepare("SELECT name FROM supermarkets");
	$supermarkets->bind_result($name);
	$supermarkets->execute();
	$supermarkets->store_result();
	
	$total_price = 0;
	$total_special = 0;
	$savings = array();
	
?>
    <?php require_once '_header.php'; ?>
    <div id="shopping-list-container">
    <div id="big_shopping_list" class="rounded-corners">
    <input type="button" id="big_shopping_list_back_button" value="Back" onclick="javascript: window.history.back(); return false;" />
    <div id="save-message"></div>
        <form action="javascript:saveShoppingList();" method="post" name="shoppinglist" id="shoppinglist-list-form">
            <fieldset>
                <legend class="form-title top-rounded-corners cart">SHOPPING LIST
					<?php 
					if ($list_archive) {
						$list_details_result->fetch();
						echo ($list_name != 'temp' && $list_name != '')? ' - <b>'.$list_name.'</b>' : '';
						echo ' ';
						echo ($list_date != '0000-00-00')? '('.date("d/m/Y", strtotime($list_date)).')' : ''; 
					}
					?>
                </legend>
                <table width="99%" cellpadding="0" cellspacing="0" align="center" class="rounded-corners">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>RRP</th>
                            <th>Special</th>
                            <th>Available At</th>
                            <th>List</th>
                            <th>Savings</th>
                            <th class="for-print">Bought</th>
                        </tr>
                    </thead>
                   
                    <tbody>
                <?php
                    if($results->num_rows > 0):
                        while($results->fetch()):
						
							//Add values for each supermarket
							$savings = store_saving($price, $special, $supermarket, $savings, $product_count);
                    		$amount_saved = number_format((($price - $special) * $product_count), 2);
                    		
                    		if (! $list_archive) {
			                    $dropdown_html = '<select name="product_count[' . $id . ']" id="product_count-' . $id . '" onchange="javascript:ChangeBigBasketProductCount(' . $id . ');">';
			                    for ($i = 1; ($i <= 20); $i++) {
			                    	$dropdown_html .= '<option value="' . $i . '"';
			                    	if ($i == $product_count) {
			                    		$dropdown_html .= ' selected';
			                    	}
			                    	$dropdown_html .= '>' . $i . '</option>';
			                    }
			                    $dropdown_html .= '</select>';
                    		} else {
                    			$dropdown_html = $product_count;
                    		}
                    ?>
                            <tr>
                                <td align="left"><?php echo $dropdown_html . 'x ' . $product_name; ?></td>
                                <td align="center">$<?php echo $price; ?></td>
                                <td align="center">$<?php echo $special; ?></td>
                                <td align="center" width="120"><?php echo $supermarket ?></td>
                                <td align="center"><button type="button" name="product[]" id="<?php echo $id; ?>" class="delete-product-button" onclick="javascript:removeFromBigBasket(<?php echo $id ?>);"><img src="images/checkbox-x.png" width="10" height="10" /></button></td>
                                <td align="center">$<?php echo $amount_saved; ?></td>
                                <td class="for-print" valign="middle" align="center"><img src="images/tickbox.jpg" /></td>
                            </tr>
                    <?php	
                            $total_price += ($product_count * $price);
                            $total_special += ($product_count * $special);
                        endwhile;
                    else:
                    ?>
                        <tr><td colspan="6" align="center" style="padding:15px 0;">There are no items in your basket</td></tr>
                    <?php
                    endif;
                ?>
                    </tbody>
                    <tfoot>
                        <?php if ($results->num_rows > 0): ?>
                            <tr>
                                <td align="center" class="border-top">Total</td>
                                <td align="center" class="border-top">$<span id="total"><?php echo number_format($total_price, 2) ?></span></td>
                                <td align="center" class="border-top">$<span id="special-total"><?php echo number_format($total_special, 2) ?></span></td>
                                <td align="center" class="border-top">&nbsp;</td>
                                <td align="center" class="border-top">&nbsp;</td>
                                <td align="center" class="border-top">&nbsp;</td>
                            </tr>
                        <?php endif; ?>
                        	<tr>
								<td colspan="6" >
                                	<table width="100%">
                        <?php foreach ($savings as $supermarket_name => $value): ?>
                            
                                    	<tr>
                                        	<td class="no-border" align="right" width="50%"><img src="images/supermarket/<?php echo strtolower($supermarket_name); ?>.png" /></td>
                                            <td class="no-border" align="left">savings of <span class="green">&nbsp;$<?php echo number_format($value, 2) ?></span>
                                            </td>
                                        </tr>
                                    
						<?php endforeach; ?>
                        			</table>
                                </td>
                            </tr>      
                        <?php if(!$list_archive): ?>
                        	<tr>
                                <td colspan="1" class="border-top select" align="left">
                                 <p>Select a supermarket <select id="select-supermarket">
                                        <option value="">----</option>
                                        <option value="">All</option>
                                    <?php 
                                        while($supermarkets->fetch()): 
                                    ?>
                                        <option value="<?php echo $name ?>" <?php echo (isset($_GET['supermarket']) && $name == $_GET['supermarket'])? "selected" : ""; ?>><?php echo $name ?></option>
                                    <?php endwhile; ?> </select></p> 
                                   </td>
                                   <td colspan="5" class="border-top buttons">                  
                                		<p>
                                			<?php 
                                			$suggested_list_name = '';
                                			if (! $list_archive) {
                                				if (($list_name != 'temp') && ($list_name != '')) {
                                					$suggested_list_name = $list_name;
                                				} else if (isset($_SESSION['list_last_saved_name']) && $_SESSION['list_last_saved_name']) {
                                					$suggested_list_name = $_SESSION['list_last_saved_name'];
                                				}
                                			}
                                			?>
                                        	<label for="shopping-list-name-input">Shopping List Name:</label><input type="text" id="shopping-list-name-input" name="shopping-list-name-input" value="<?php echo htmlentities($suggested_list_name); ?>" />
                                            <input type="hidden" id="user-id-input" name="user-id-input" value="<?php echo $user->getId();?>" />
                                        	<input type="submit" id="save" name="save" value="SAVE" />
                                        	<input type="button" id="print" name="print" value="PRINT" onclick="javascript: window.print(); return false;" />
                                        </p>
                           		</td>
                            </tr>
                        <?php else: ?>
                        	<tr>
                            	<td colspan="3" class="border-top select">&nbsp;
                                	
                                </td>
                                <td colspan="3" class="border-top buttons">
                                	<input type="button" id="delete" name="delete" value="DELETE" onClick="javascript:deleteShoppingList(<?php echo $list_archive; ?>); return false;" />
                                	<input type="button" id="find-sales-again" name="find-sales-again" value="FIND SALES AGAIN" onClick="javascript:findSalesAgain(<?php echo $list_archive; ?>); return false;" />
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tfoot>
       				</table>
                </fieldset>
            </form>
    	<script>
	    // Basically, if we are going to overwrite a shopping list with the same date, that isn't the
	    // last one we saved, we should confirm the overwrite...
	    shopping_lists_to_confirm_overwrite = [];
	    <?php
	    // The 'date' of the list is just the date that it was saved? So for now, let's just look at the last 7 days...
	    $overwrite_lists_query = "SELECT name 
	    							FROM shopping_lists 
	    							WHERE `date`>=DATE_SUB(CURDATE(), INTERVAL 7 DAY) 
	    								AND user_id=" . intval($user->getId()) . "
	    							ORDER BY creation_time DESC";
	    $overwrite_lists_result =  $db_link->query($overwrite_lists_query);
	    $lists_to_confirm = array();
	    
	    // We don't confirm overwriting the last list...
	    $has_skipped_last = false;
	    while ($overwrite_lists_row = $overwrite_lists_result->fetch_assoc()) {
			if (! $has_skipped_last) {
				$has_skipped_last = true;
				continue;
			}
				
			$overwrite_list_name = $overwrite_lists_row['name'];
			$overwrite_list_name = strtolower($overwrite_list_name);
			$overwrite_list_name = trim($overwrite_list_name);
			$overwrite_list_name = str_ireplace("\\", "\\\\", $overwrite_list_name);
			$overwrite_list_name = str_ireplace("'", "\\'", $overwrite_list_name);
			
			$overwrite_list_name = "'" . $overwrite_list_name . "'";
			if (! in_array($overwrite_list_name, $lists_to_confirm)) {
				$lists_to_confirm[] = $overwrite_list_name;
			}
		}
		?>
	    shopping_lists_to_confirm_overwrite = [<?php echo implode(', ', $lists_to_confirm); ?>];
		</script>
    </div>
</div>
<?php require_once '_footer.php'; ?>
