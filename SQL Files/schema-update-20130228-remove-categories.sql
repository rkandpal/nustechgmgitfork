#
# Product types table
#

# Add the aisle_id column, with its keys
ALTER TABLE `product_types`
	ADD COLUMN `aisle_id` INT(3) NULL DEFAULT NULL AFTER `name`,
	ADD INDEX `aisle_id` (`aisle_id`),
	ADD CONSTRAINT `aisle_id_fk` FOREIGN KEY (`aisle_id`) REFERENCES `aisle` (`id`) ON UPDATE SET NULL ON DELETE SET NULL;

# Set the aisle_id values based on the current category_id
UPDATE `product_types`
	INNER JOIN `category` ON `category`.`id`=`product_types`.`category_id`
	SET `product_types`.`aisle_id`=`category`.`aisle_id`;

# Drop the keys on the category_id column
ALTER TABLE `product_types`
	DROP FOREIGN KEY `category_id`;

ALTER TABLE `product_types`
	DROP KEY `category_id`;

# Drop this index. We'll add it back in once we've fixed the collisions in the uniqueness
ALTER TABLE `product_types`
	DROP INDEX `name`;

# Finally, drop the category_id column
ALTER TABLE `product_types`
	DROP COLUMN `category_id`;

#
# Products table
#

# Add the aisle_id column
ALTER TABLE `products`
	ADD COLUMN `aisle_id` INT(3) NULL DEFAULT NULL AFTER `category_id`;

# Set the aisle_id values based on the current category_id
UPDATE `products`
	INNER JOIN `category` ON `category`.`id`=`products`.`category_id`
	SET `products`.`aisle_id`=`category`.`aisle_id`;

# Finally, drop the category_id column
ALTER TABLE `products`
	DROP COLUMN `category_id`;


#
# Update the CRUD screens
#

# Remove the categories module and its fields
DELETE FROM `modules` WHERE `id`=66 AND `name`='Categories';
DELETE FROM `modules_fields` WHERE `module_id`=66;

# Change products module to use aisles
UPDATE `modules_fields` 
	SET `name`='aisle_id', `label`='Aisle', `type`='aisle'
	WHERE `module_id`=69 AND `name`='category_id';

# Change product types module to use aisles
UPDATE `modules_fields` 
	SET `name`='aisle_id', `label`='Aisle', `type`='aisle'
	WHERE `module_id`=70 AND `name`='category_id';

# Change the Aisle CRUD screen to be its own menu item
UPDATE `modules`
	SET `parent_module_id`=0
	WHERE `id`=67 AND `name`='Aisles';
	
#
# Finally, drop the category table
#
DROP TABLE `category`;


