<?php
require_once(dirname(__FILE__).'/../functions.php');
require_once(dirname(__FILE__).'/../config.php');

$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);

echo '<pre>';
echo "Get available postcodes for each supermarket...\n";
$query = "SELECT supermarket_id, postcode FROM stores;";
$result = $db_link->query($query);

$supermarket_postcodes = array();
while ($row = $result->fetch_assoc()) {
	$supermarket_id = intval($row['supermarket_id']);
	$postcode = intval($row['postcode']);
	
	if (! isset($supermarket_postcodes[$supermarket_id])) {
		$supermarket_postcodes[$supermarket_id] = array();
	}
	
	if (! $postcode) {
		$postcode = 2000;
	}
	
	if (! in_array($postcode, $supermarket_postcodes[$supermarket_id])) {
		$supermarket_postcodes[$supermarket_id][] = $postcode;
	}
}

echo "Determining if there are any conflicts with states and supermarkets...\n";
foreach ($supermarket_postcodes as $supermarket_id => $postcodes) {
	$state_codes = array();
	foreach ($postcodes as $postcode) {
		$postcode = intval(substr($postcode, 0, 1));
		if (! in_array($postcode, $state_codes)) {
			$state_codes[] = $postcode;
		}
	}
	
	if (count($state_codes) > 1) {
		echo "!! Supermarket $supermarket_id has stores in more than one state\n";
	}
}

echo "Deciding on which postcode to use for previous sales for each supermarket...\n";
foreach ($supermarket_postcodes as $supermarket_id => &$postcodes) {
	$postcodes = array_shift($postcodes);
}
unset($postcodes);

echo "Postcodes to use for previous sales for each supermarket...\n";
foreach ($supermarket_postcodes as $supermarket_id => $postcode) {
	echo $supermarket_id . ': ' . $postcode . "\n";
}

echo "Adding a postcode to all sales for each supermarket...\n";
foreach ($supermarket_postcodes as $supermarket_id => $postcode) {
	$query = "UPDATE product_sales SET postcode=$postcode WHERE supermarket_id=$supermarket_id";
	$db_link->query($query);
	echo $query . ': ' . $db_link->affected_rows . "\n";
}

echo "Adding a postcode to all shopping list products for each supermarket...\n";
foreach ($supermarket_postcodes as $supermarket_id => $postcode) {
	$query = "UPDATE shopping_lists_products SET postcode=$postcode WHERE supermarket_id=$supermarket_id";
	$db_link->query($query);
	echo $query . ': ' . $db_link->affected_rows . "\n";
}

echo "DONE!\n";