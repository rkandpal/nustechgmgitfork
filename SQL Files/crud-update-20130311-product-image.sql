# Change the type of the product image
UPDATE modules_fields 
	INNER JOIN modules ON modules.id=modules_fields.module_id
	SET modules_fields.type='file_product_image'
	WHERE modules.name='Products'
	AND modules_fields.name='image'
	AND modules_fields.type='file_imageXX';

