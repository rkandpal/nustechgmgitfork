<?php
require_once(dirname(__FILE__).'/../functions.php');
require_once(dirname(__FILE__).'/../config.php');

$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
echo "<pre>\n";

/*
 * Here is a map of tables to columns: any column needs to be tested for bad smart 
 * quotes...
 */
$check_map = array(
		'aisle' => array('aisle_name'),
		'brands' => array('name'),
		'products' => array('name', 'technical_name', 'description'),
		'product_types' => array('name'),
		'shopping_lists_products' => array('name', 'description')
	);

foreach ($check_map as $table_name => $columns) {
	echo "Checking table $table_name... ";
	$query = "SELECT * FROM $table_name";
	$db_result = $db_link->query($query);
	echo "Checking " . $db_result->num_rows . " rows\n";
	
	$line_counter = 0;
	while ($db_row = $db_result->fetch_assoc()) {
		$line_counter++;
		$must_change_row = false;
		foreach ($columns as $column) {
			if ($db_row[$column] != RewriteSmartQuotes($db_row[$column])) {
				$must_change_row = true;
			}
		}
		
		if (! $must_change_row) {
			echo '.';
		} else {
			$query = "UPDATE $table_name SET ";
			
			$column_updates = array();
			foreach ($columns as $column) {
				$this_update = "`$column`='";
				$this_update .= $db_link->real_escape_string(RewriteSmartQuotes($db_row[$column]));
				$this_update .= "'";
				
				$column_updates[] = $this_update;
			}
			
			$query .= implode(', ', $column_updates);
			$query .= " WHERE `id`=" . $db_row['id'];
			$update_result = $db_link->query($query);
			
			if ($update_result && ($db_link->affected_rows == 1)) {
				echo 'X';
			} else {
				die($query);
			}
		}
		
		if (($line_counter % 100) == 0) {
			echo "\n";
		}
	}
	
	echo "\nDone with table $table_name.\n";
}

echo "\nDONE!\n";

