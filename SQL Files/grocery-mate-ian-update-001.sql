-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2012-07-30 20:41:53
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping structure for table grocery_mate.modules
DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `table` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `parent_module_id` int(16) NOT NULL,
  `field_uid` int(16) NOT NULL,
  `field_slug` int(16) NOT NULL,
  `field_parent` int(16) NOT NULL,
  `field_orderby` int(16) NOT NULL,
  `orderby_direction` enum('DESC','ASC') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DESC',
  `management_width` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locked` tinyint(1) NOT NULL,
  `lock_records` tinyint(1) NOT NULL,
  `core_module` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table grocery_mate.modules: 8 rows
DELETE FROM `modules`;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `name`, `table`, `slug`, `parent_module_id`, `field_uid`, `field_slug`, `field_parent`, `field_orderby`, `orderby_direction`, `management_width`, `type`, `locked`, `lock_records`, `core_module`) VALUES
	(3, 'Modules', 'modules', 'modules', 0, 6, 14, 10, 14, 'ASC', '30%', 'module', 1, 1, 1),
	(4, 'Fields', 'modules_fields', 'fields', 3, 17, 20, 0, 19, 'DESC', '30%', 'module_field', 1, 1, 1),
	(5, 'Validation', 'modules_fields_validation', 'validation', 3, 39, 40, 0, 39, 'DESC', '30%', 'module_field_validation', 1, 1, 1),
	(62, 'Users', 'users', 'users', 0, 362, 371, 0, 0, 'DESC', '20%', 'user', 0, 0, 0),
	(63, 'Groups', 'users_groups', 'groups', 62, 379, 380, 0, 0, 'ASC', '20%', 'user_group', 0, 0, 0),
	(64, 'Meta', 'users_meta', 'meta', 62, 384, 385, 0, 0, 'DESC', '20%', 'user_meta', 0, 0, 0),
	(65, 'Backups', 'backups', 'backups', 0, 388, 390, 0, 0, 'DESC', '20%', 'backup', 0, 0, 0),
	(66, 'Categories', 'category', 'categories', 0, 391, 392, 0, 0, 'DESC', '20%', 'category', 0, 0, 0),
	(67, 'Aisles', 'aisle', 'aisles', 66, 394, 395, 0, 0, 'DESC', '20%', 'aisle', 0, 0, 0),
	(69, 'Products', 'products', 'products', 0, 398, 405, 0, 0, 'DESC', '20%', 'product', 0, 0, 0),
	(68, 'Brands', 'brands', 'brands', 0, 396, 397, 0, 0, 'DESC', '20%', 'brand', 0, 0, 0),
	(70, 'Product Types', 'product_types', 'product-types', 69, 406, 407, 0, 0, 'DESC', '20%', 'product_type', 0, 0, 0),
	(71, 'Similar Products', 'products_similar', 'similar-products', 69, 408, 409, 0, 0, 'DESC', '20%', 'similar_product', 0, 0, 0),
	(72, 'Product Sales', 'product_sales', 'product-sales', 69, 411, 412, 0, 0, 'DESC', '20%', 'product_sales', 0, 0, 0),
	(73, 'Product Sale Exceptions', 'product_sale_exceptions', 'product-sale-exceptions', 69, 413, 423, 0, 0, 'DESC', '20%', 'product_sale_exception', 0, 0, 0),
	(74, 'Stores', 'stores', 'stores', 0, 425, 426, 0, 0, 'DESC', '20%', 'store', 0, 0, 0),
	(75, 'Supermarket', 'supermarkets', 'supermarkets', 74, 431, 432, 0, 0, 'DESC', '20%', 'supermarket', 0, 0, 0);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;


-- Dumping structure for table grocery_mate.modules_fields
DROP TABLE IF EXISTS `modules_fields`;
CREATE TABLE IF NOT EXISTS `modules_fields` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `order` tinyint(2) NOT NULL,
  `module_id` int(32) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `editable` tinyint(1) NOT NULL,
  `display_width` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `tooltip` text COLLATE utf8_unicode_ci NOT NULL,
  `fieldset` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `specific_search` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=433 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table grocery_mate.modules_fields: 61 rows
DELETE FROM `modules_fields`;
/*!40000 ALTER TABLE `modules_fields` DISABLE KEYS */;
INSERT INTO `modules_fields` (`id`, `order`, `module_id`, `name`, `label`, `type`, `editable`, `display_width`, `tooltip`, `fieldset`, `specific_search`) VALUES
	(6, 1, 3, 'id', 'Id', 'id', 0, '10%', '', '', 0),
	(8, 3, 3, 'table', 'Table', '', 1, '', '', '', 0),
	(9, 6, 3, 'slug', 'Slug', '', 1, '', '', '', 0),
	(10, 7, 3, 'parent_module_id', 'Module Parent', 'module', 1, '', '', '', 0),
	(11, 8, 3, 'field_uid', 'Id field', 'module_field_current', 1, '', '', '', 0),
	(12, 12, 3, 'orderby_direction', 'Default order direction', 'orderby_direction', 1, '', '', '', 0),
	(13, 13, 3, 'management_width', 'Options field width', 'text_small', 1, '', '', '', 0),
	(14, 2, 3, 'name', 'Name', '', 1, '55%', '', '', 0),
	(15, 10, 3, 'field_parent', 'Parent field', 'module_field_current', 1, '', '', '', 0),
	(16, 9, 3, 'field_slug', 'Slug field', 'module_field_current', 1, '', '', '', 0),
	(25, 11, 3, 'field_orderby', 'Default order by field', 'module_field_current', 1, '', '', '', 0),
	(35, 9, 3, 'type', 'Type definition', '', 1, '', '', '', 0),
	(50, 10, 3, 'locked', 'Locked', 'yes_no', 1, '', '', '', 0),
	(94, 10, 3, 'core_module', 'Core module', 'yes_no', 1, '', '', '', 0),
	(91, 9, 3, 'lock_records', 'Lock Records', 'yes_no', 1, '', '', '', 0),
	(17, 1, 4, 'id', 'ID', 'id', 0, '10%', '', '', 0),
	(18, 3, 4, 'name', 'Name', '', 1, '', '', '', 0),
	(19, 6, 4, 'module_id', 'Module', 'module', 1, '30%', '', '', 1),
	(20, 2, 4, 'label', 'Label', '', 1, '30%', '', '', 0),
	(21, 5, 4, 'type', 'Type', 'type', 1, '', '', '', 0),
	(22, 8, 4, 'editable', 'Editable?', 'yes_no', 1, '', '', '', 0),
	(23, 7, 4, 'display_width', 'Display width', 'text_small', 1, '', '', '', 0),
	(34, 4, 4, 'order', 'Order', 'text_small', 1, '', '', '', 0),
	(312, 2, 4, 'tooltip', 'Tooltip', 'textarea_small', 1, '', '', '', 0),
	(321, 3, 4, 'fieldset', 'Fieldset', '', 1, '', 'Fields of the same fieldset will be grouped together', '', 0),
	(327, 10, 4, 'specific_search', 'Specific search', 'yes_no', 1, '', 'If selected this column will be given it\'s own search field in forms', '', 0),
	(39, 1, 5, 'id', 'ID', 'id', 0, '10%', '', '', 0),
	(40, 2, 5, 'name', 'Rule Name', 'module_validation_rule', 1, '30%', '', '', 0),
	(43, 4, 5, 'field_id', 'Field', 'module_field', 1, '30%', '', '', 0),
	(362, 1, 62, 'id', 'ID', 'id', 0, '', '', '', 0),
	(363, 1, 62, 'email', 'Email', '', 1, '45%', '', '', 0),
	(364, 0, 62, 'email_verified', 'Email Verified', 'yes_no', 1, '', 'If <a href="?module_path=dashboard/settings">Email Verification</a> is enabled then this shows whether or not the user has verified their email address.', '', 1),
	(365, 4, 62, 'password', 'Password', 'password', 1, '', '', '', 0),
	(366, 8, 62, 'lastlogin', 'Last login', 'datetime_static', 1, '', '', '', 0),
	(367, 12, 62, 'lastip', 'Last IP used', 'static', 1, '', '', '', 0),
	(368, 5, 62, 'group_id', 'User Group', 'user_group', 1, '', '', '', 0),
	(369, 11, 62, 'avatar', 'Profile image', 'file_image', 1, '', '', '', 0),
	(370, 7, 62, 'date_registered', 'Date registered', 'datetime_static', 1, '30%', '', '', 0),
	(371, 1, 62, 'display_name', 'Display name', '', 1, '', '', '', 0),
	(372, 5, 62, 'temporary_password', 'Temporary password', 'hidden', 0, '', '', '', 0),
	(373, 12, 62, 'facebook_id', 'Facebook ID', 'static', 1, '', 'If the user has linked their account with Facebook or uses Facebook to login, this is their Facebook account ID.', '', 0),
	(374, 13, 62, 'twitter_id', 'Twitter ID', 'static', 1, '', 'If the user has linked their account with Twitter or uses Twitter to login, this is their Twitter account ID.', '', 0),
	(375, 14, 62, 'yahoo_id', 'Yahoo ID', 'static', 1, '', 'If the user has linked their account with Yahoo or uses Yahoo to login, this is their Yahoo account ID.', '', 0),
	(376, 15, 62, 'windows_live_id', 'Windows Live ID', 'static', 1, '', 'If the user has linked their account with Windows Live or uses Windows Live to login, this is their Windows Live account ID.', '', 0),
	(377, 16, 62, 'google_id', 'Google ID', 'static', 1, '', 'If the user has linked their account with Google or uses Google to login, this is their Google account ID.', '', 0),
	(378, 0, 62, 'type', 'Type', 'user_type', 0, '1%', '', '', 0),
	(379, 1, 63, 'id', 'ID', 'id', 0, '', '', '', 0),
	(380, 2, 63, 'name', 'Name', '', 1, '30%', '', '', 0),
	(381, 3, 63, 'admin', 'Users are Admins?', 'yes_no', 1, '', '', '', 0),
	(382, 4, 63, 'default_value', 'Default group', 'yes_no', 1, '', '', '', 0),
	(383, 5, 63, 'access_level', 'Access Level', 'integer', 1, '', '', '', 0),
	(384, 1, 64, 'id', 'ID', 'id', 0, '', '', '', 0),
	(385, 2, 64, 'key', 'Key', '', 1, '20%', '', '', 0),
	(386, 3, 64, 'value', 'Value', 'textarea_small', 1, '', '', '', 0),
	(387, 4, 64, 'user', 'User', 'user', 1, '30%', '', '', 0),
	(388, 1, 65, 'id', 'ID', 'id', 0, '', '', '', 0),
	(389, 2, 65, 'date_time', 'Date & Time', 'datetime_now', 1, '40%', '', '', 0),
	(390, 3, 65, 'file', 'File', 'file', 1, '', '', '', 0),
	(391, 1, 66, 'id', 'ID', 'id', 0, '', '', '', 0),
	(392, 2, 66, 'category_name', 'Name', '', 1, '50%', '', '', 0),
	(393, 3, 66, 'aisle_id', 'Aisle', 'aisle', 1, '', '', '', 0),
	(394, 1, 67, 'id', 'Aisle', 'id', 0, '', '', '', 0),
	(395, 2, 67, 'aisle_name', 'Name', '', 1, '50%', '', '', 0),
	(396, 1, 68, 'id', 'Brand', 'id', 0, '', '', '', 0),
	(397, 2, 68, 'name', 'Name', '', 1, '50%', '', '', 0),
	(398, 1, 69, 'id', 'Product', 'id', 0, '', '', '', 0),
	(399, 2, 69, 'type', 'Type', 'type', 1, '20%', '', '', 0),
	(400, 3, 69, 'ean', 'EAN', '', 1, '20%', '', '', 0),
	(401, 4, 69, 'brand', 'Brand', 'brand', 1, '20%', '', '', 0),
	(402, 5, 69, 'measure', 'Measure', '', 1, '20%', '', '', 0),
	(403, 6, 69, 'uom', 'UOM', '', 1, '20%', '', '', 0),
	(404, 7, 69, 'image', 'Image', 'file_image', 1, '', '', '', 0),
	(405, 8, 69, 'category_id', 'Category', 'category', 1, '20%', '', '', 0),
	(406, 1, 70, 'id', 'Type', 'id', 0, '', '', '', 0),
	(407, 2, 70, 'name', 'Name', '', 1, '50%', '', '', 0),
	(408, 1, 71, 'id', 'ID', 'id', 0, '', '', '', 0),
	(409, 2, 71, 'product_id', 'Product #1', 'product', 1, '50%', '', '', 0),
	(410, 3, 71, 'similar_to_product_id', 'Product #2', 'product', 1, '50%', '', '', 0),
	(411, 1, 72, 'id', 'ID', 'id', 0, '', '', '', 0),
	(412, 2, 72, 'product_id', 'Product', 'product', 1, '20%', '', '', 0),
	(413, 3, 72, 'supermarket_id', 'Supermarket', 'supermarket', 1, '20%', '', '', 0),
	(414, 4, 72, 'start_date', 'Start Date', 'date', 1, '20%', '', '', 0),
	(415, 5, 72, 'end_date', 'End Date', 'date', 1, '20%', '', '', 0),
	(416, 6, 72, 'price', 'Price', '', 1, '', '', '', 0),
	(417, 7, 72, 'special_price', 'Special Price', '', 1, '20%', '', '', 0),
	(418, 8, 72, 'bogo_buy_count', 'BOGO Buy Count', 'integer', 1, '', '', '', 0),
	(419, 9, 72, 'bogo_buy_price', 'BOGO Buy Price', '', 1, '', '', '', 0),
	(420, 10, 72, 'bogo_get_count', 'BOGO Get Count', 'integer', 1, '', '', '', 0),
	(421, 11, 72, 'bogo_get_price', 'BOGO Get Price', '', 1, '', '', '', 0),
	(422, 1, 73, 'id', 'ID', 'id', 0, '', '', '', 0),
	(423, 2, 73, 'product_sale_id', 'Product', 'product_sale', 1, '50%', '', '', 0),
	(424, 3, 73, 'exception_store_id', 'Store', 'store', 1, '50%', '', '', 0),
	(425, 1, 74, 'id', 'ID', 'id', 0, '', '', '', 0),
	(426, 2, 74, 'supermarket_id', 'Supermarket', 'supermarket', 1, '', '', '', 0),
	(427, 3, 74, 'suburb', 'Suburb', '', 1, '', '', '', 0),
	(428, 4, 74, 'postcode', 'Postcode', '', 1, '', '', '', 0),
	(429, 5, 74, 'latitude', 'Latitude', '', 1, '', '', '', 0),
	(430, 6, 74, 'longitude', 'longitude', '', 1, '', '', '', 0),
	(431, 1, 75, 'id', 'ID', 'id', 0, '', '', '', 0),
	(432, 1, 75, 'name', 'Name', '', 1, '50%', '', '', 0);
/*!40000 ALTER TABLE `modules_fields` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
