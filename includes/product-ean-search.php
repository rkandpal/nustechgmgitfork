<script>
function DisplayProductTypeDivAndProductType(type_id) {
	$('#browes-categories').css('display', '');
	getProducts(type_id, false);
	$('.browse-list').remove();
}

$('#search-input').live('keyup', function(event) {
	var search_value = $('#search-input').val();
	
	setTimeout(function() {
		if ($('#search-input').val() == search_value) {
			$('#searching').css('display', '');
			
			$('#search-brands').css('display', 'none');
			$('#search-results-brand-header').css('display', 'none');
			$('#search-results-brand-results').css('display', 'none');
			
			$('#search-product-type').css('display', 'none');
			$('#search-results-product-type-header').css('display', 'none');
			$('#search-results-product-type-results').css('display', 'none');

			$('#search-product').css('display', 'none');
			$('#search-results-product-header').css('display', 'none');
			$('#search-results-product-results').css('display', 'none');
							
			searchRequest = $.ajax({
				url: '../search-ajax.php',
				type: 'POST',
				dataType: 'json',
				data: { search: $('#search-input').val(),
						ean_search: true
					},
				success: function(data) {
					if (data.status == 'fail') {
						alert(data.status_text);
					} else {
						$('#searching').css('display', 'none');
						$('#search-product').css('display', 'inline-block');
						$('#search-results-product-header').css('display', '');
						$('#search-results-product-results').css('display', '');
						
						if (data.data.product.length) {
							var html = '';
							var last_match_type = '';
							
							for (var i in data.data.product) {
								var the_item = data.data.product[i];
								var this_match_type = the_item.match;

								if (this_match_type != last_match_type) {
									if (this_match_type == 'phrase') {
										html += '<br /><strong>Matching Phrase</strong><br />';
									} else if (this_match_type == 'all') {
										html += '<br /><strong>Matching All Words</strong><br />';
									} else if (this_match_type == 'any') {
										html += '<br /><strong>Matching Any Words</strong><br />';
									}

									last_match_type = this_match_type;
								}
								
								html += '<a href="../product.php?view=VIEW&pid=' + the_item.id + '">';
								html += ((the_item.name == '') ? 'Unknown Product' : the_item.name);
								html += '</a>: <span class="ean">' + the_item.ean + '</span><br />';
							}
							$('#search-results-product-results').html(html);
						} else {
							$('#search-results-product-results').html('No matching products found');
						}
						// alert(data);
					}
					// $('body').append('<div>'+ data +'</div>');
				}	
			});
		}
	}, 500);
});
</script>

<div id="search">
    <form action="javascript: return false;" class="standard" method="post" name="search" id="search-form">
        <input type="text" name="search-input" class="data input-text" id="search-input" placeholder="Product Search" size="40" />
    </form>
</div><!-- #search -->
<div id="search-results" class="rounded-corners">
    <div id="searching" style="display: none;">Searching...</div>
    <div id="search-product" class="rounded-corners" style="display:none">
    	<div id="search-results-product-header" style="display: none;">Matching Products</div>
    	<div id="search-results-product-results" style="display: none;"></div>
    </div>
</div>
