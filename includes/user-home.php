
<div id="user-home">
    <div id="user-panel">
        <h2 id="welcome-user">
            Welcome <?php print_user_detail('name') ?>
        </h2>
        <div id="user-details" class="rounded-corners">
            <h4>MY DETAILS</h4>
            <ul>
                <li>Name: <em><?php print_user_detail('name'); ?></em></li>
                <li>Email: <em><?php print_user_detail('email'); ?></em></li>
                <li>Password: <em><?php print_user_detail('password'); ?></em></li>
            </ul>
        </div><!-- #user-details -->
    </div><!-- #user-panel -->
    <div id="shoppinglist-list" class="rounded-corners">
    	
        <?php get_shoppinglist_list() ?>
    </div><!-- #shoppinglist-list -->
</div><!-- #user-home -->

