<?php
	require_once(dirname(__FILE__).'/../functions.php');
	require_once(dirname(__FILE__).'/../config.php');
	
	StartSession();
	$output = array();
	$output['result'] = false;
	$output['products_copied'] = 0;

	if ($_SESSION['user_id'] && $_POST['list_id']) {
		$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		$list_id = intval($_POST['list_id']);
		$user_id = intval($_SESSION['user_id']);
		
		// Does this list belong to this user?
		$query = "SELECT id FROM shopping_lists WHERE id=$list_id AND user_id=$user_id";
		$result = $db_link->query($query);
		
		if ($result->num_rows == 1) {
			$output['result'] = true;
			$current_list_id = intval(isset($_SESSION['list_id']) ? $_SESSION['list_id'] : get_last_id());

			// Which items are we already getting?
			$current_list_products = array();
			$query = "SELECT DISTINCT(shopping_list_product_id) FROM shopping_lists_products WHERE shopping_list_id=$current_list_id";
			$result = $db_link->query($query);
			
			while ($row = $result->fetch_assoc()) {
				$this_product_id = intval($row['shopping_list_product_id']);
				if (! in_array($this_product_id, $current_list_products)) {
					$current_list_products[] = $this_product_id;
				}
			}
			
			// Which items were on the list we're copying?
			$query = "SELECT shopping_list_product_count, shopping_list_product_id, shopping_list_product_sale_id, supermarket_id 
						FROM shopping_lists_products
						WHERE shopping_list_id=$list_id";
			
			$copy_result = $db_link->query($query);
			
			while ($copy_row = $copy_result->fetch_assoc()) {
				$copy_row_product_count = intval($copy_row['shopping_list_product_count']);
				$copy_row_product_id = intval($copy_row['shopping_list_product_id']);
				$copy_row_product_sale_id = intval($copy_row['shopping_list_product_sale_id']);
				$copy_row_supermarket_id = intval($copy_row['supermarket_id']);
				
				// Are we already buying this product?
				if (in_array($copy_row_product_id, $current_list_products)) {
					continue;
				}
				
				// Are there any sales on at the moment for this product?
				$supermarkets_with_product = array();
				
				$query = "SELECT DISTINCT(supermarket_id) FROM product_sales 
							WHERE product_id=$copy_row_product_id
								AND start_date <= CURDATE()
								AND end_date >= CURDATE()";
				
				$supermarket_search_result = $db_link->query($query);
				while ($supermarket_search_row = $supermarket_search_result->fetch_assoc()) {
					$supermarkets_with_product[] = intval($supermarket_search_row['supermarket_id']);
				}
				
				if (! count($supermarkets_with_product)) {
					continue;
				} 
				
				// Values that we will eventually put into the sale table
				$new_sale_id = null;
				$the_store_to_buy_from = null;
				$new_sale_postcode = null;
				$new_sale_price = null;
				$new_special_price = null;
				
				// Maybe this exact sale is still available?
				$query = "SELECT * FROM product_sales WHERE id=$copy_row_product_sale_id AND start_date <= CURDATE() AND end_date >= CURDATE()";
				$same_sale_search_result = $db_link->query($query);
				if ($same_sale_search_result->num_rows == 1) {
					$same_sale_search_row = $same_sale_search_result->fetch_assoc();
					$new_sale_id = $same_sale_search_row['id'];
					$the_store_to_buy_from = $same_sale_search_row['supermarket_id'];
					$new_sale_postcode = $same_sale_search_row['postcode'];
					$new_sale_price = $same_sale_search_row['price'];
					$new_special_price = $same_sale_search_row['special_price'];					
				} else {
					// Where are we going to get this sale from?
					// If the sale is on at the original store, we will take it...
					if (in_array($copy_row_supermarket_id, $supermarkets_with_product)) {
						$the_store_to_buy_from = $copy_row_supermarket_id;
					} else {
						// Just take the first one...
						$the_store_to_buy_from = array_shift($supermarkets_with_product);
					}
					
					// So let's get the relevant row...
					$query = "SELECT * FROM product_sales WHERE product_id=$copy_row_product_id AND supermarket_id=$the_store_to_buy_from AND start_date <= CURDATE() AND end_date >= CURDATE()";
					$the_new_sale_result = $db_link->query($query);
					$the_new_sale_row = $the_new_sale_result->fetch_assoc();
					
					$new_sale_id = $the_new_sale_row['id'];
					$new_sale_postcode = $the_new_sale_row['postcode'];
					$new_sale_price = $the_new_sale_row['price'];
					$new_special_price = $the_new_sale_row['special_price'];
				}
				
				// Get the product that we are adding...
				$query = "SELECT * FROM products WHERE id=$copy_row_product_id";
				$the_product_result = $db_link->query($query);
				$the_product_row = $the_product_result->fetch_assoc();
				
				$the_product_type = $the_product_row['type'];
				$the_product_brand = $the_product_row['brand'];
				$the_product_measure = $the_product_row['measure'];
				$the_product_uom = $db_link->real_escape_string($the_product_row['uom']);
				$the_product_image = $db_link->real_escape_string($the_product_row['image']);
				$the_product_name = $db_link->real_escape_string($the_product_row['name']);
				$the_product_description = $db_link->real_escape_string($the_product_row['description']);
				
				// Add the product to the new list...
				$query = "INSERT INTO shopping_lists_products
								(shopping_list_product_count, shopping_list_product_id, shopping_list_product_sale_id, shopping_list_id,
								supermarket_id, postcode, price, special_price, type, brand,
								measure, uom, image, name, description)
							VALUES ($copy_row_product_count, $copy_row_product_id, $new_sale_id, $current_list_id,
								$the_store_to_buy_from, $new_sale_postcode, $new_sale_price, $new_special_price, $the_product_type, $the_product_brand,
								$the_product_measure, '$the_product_uom', '$the_product_image', '$the_product_name', '$the_product_description');";
				
				$db_link->query($query);

				// And finally update the list totals...
				$query = "UPDATE shopping_lists SET total_rrp = total_rrp + ($copy_row_product_count * $new_sale_price) WHERE id = $current_list_id;";
				$db_link->query($query);
				
				$query = "UPDATE shopping_lists SET total_special = total_special + ($copy_row_product_count * $new_special_price) WHERE id = $current_list_id;";
				$db_link->query($query);
				
				$output['products_copied']++;
			}
		}
	}

	echo json_encode($output);
?>