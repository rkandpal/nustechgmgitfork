<?php
	require_once(dirname(__FILE__).'/../functions.php');
	require_once('../config.php');
	
	$db_link = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME) 
		or die("There was an error connecting to the database: ".$db_link->error);
	$brandID = intval($_POST['brandID']);
	$now_date_string = date('Y-m-d');
	
	$query = "SELECT products.id, product_sales.id, products.measure, products.uom, products.image,
					 product_sales.price, product_sales.special_price, product_sales.end_date,
					 brands.name, products.name, products.technical_name,
					 product_types.name,
					 supermarkets.name,
					 aisle.id AS aisle_id, aisle_name 
			  FROM products, product_sales, brands, product_types, supermarkets, aisle 
			  WHERE products.brand = brands.id AND 
			  		brands.id=$brandID AND
					product_sales.product_id = products.id AND
					products.type = product_types.id AND
					product_types.aisle_id = aisle.id AND
					product_sales.supermarket_id = supermarkets.id AND
					product_sales.start_date<='$now_date_string' AND 
					product_sales.end_date>='$now_date_string'";
	
	$results = $db_link->prepare($query);

		$results->bind_result($id, $sale_id, $measure, $uom, $image, $price, $special, $sale_end_date, $brand, $name, $technical_name, $product_type, $supermarket, $aisle_id, $aisle_name);
		$results->execute();
		$results->store_result();
		$row_cnt = $results->num_rows;
		
		$odd_even = "odd";
	$output = '';
	//echo $row_cnt;
	if(empty($row_cnt)):
		$output['result'] = false;
		$output['html'] = '<p>Sorry there are no products of this brand on special</p>';
	else :
		$output['result'] = true;

		$output['html'] = '';
		$first_row = true;
		while($results->fetch())
		{	
			$product_image = ($image != '')?$image:"default_product.jpg";
			if ($first_row) {
				// $output['html'] .= '<div class="hidden" id="product-type">'.ucwords($category).' &gt; '.ucwords($product_type).'</div>';
				$output['html'] .= '<div class="hidden" id="product-type">'.ucwords($brand).'</div>';
				$first_row = false;
			}

			$output['html'] .= '<div class="product rounded-corners '.$odd_even.'">';
			$output['html'] .= '<table cellpadding="0" cellspacing="0" width="100%">';
			$output['html'] .= '	<tr>';
			$output['html'] .= '		<td class="image"><img class="product-thumb rounded-corners" src="images/'.$product_image.'" style="vertical-align:top" width="130" /></td>';
			$output['html'] .= '		<td class="product-data">';
			$output['html'] .= '			<table cellpadding="0" cellspacing="0" width="100%">';
			$output['html'] .= '				<tr><td class="product-name">'.ucwords($brand . ' ' . ($name ? $name : $technical_name)).'</td>';
			$output['html'] .= '				<tr><td class="weight">'.round($measure,2) . '' . $uom .'</td></tr>';
			$output['html'] .= '				<tr><td class="price">RRP: $' . $price . '</td></tr>';
			$output['html'] .= '				<tr><td class="special">Special: $'.$special.'</td></tr>';	
			
			$formatted_sale_date = GetFormattedDBDate($sale_end_date);
			if ($formatted_sale_date) {
				$output['html'] .= '				<tr><td class="sale-date">On sale until ' . $formatted_sale_date . '</td></tr>';
			}
			
			$output['html'] .= 				'</table>';
			$output['html'] .= '		</td>';
			$output['html'] .= '	</tr>';
			$output['html'] .= '	<tr>';
			$output['html'] .= '		<td class="supermarket"><img src="images/'.strtolower($supermarket).'.png" width="80"/></td>';
			$output['html'] .= ' 		<td class="forms"><div class="product-form">';
			$output['html'] .= '				<form class="view-product" name="view-product" id="view-'.$id.'" method="get" action="product.php">';
			$output['html'] .= '					<input type="submit" class="rounded-corners" name="view" id="view-'.$id.'" value="VIEW" />';
			$output['html'] .= '					<input type="hidden" name="pid" value="'.$id.'" />';
			$output['html'] .= '				</form>';
			$output['html'] .= '				<form class="add-to-basket" name="add-to-basket" onsubmit="javascript:addToBasket('.$sale_id.'); return false;">';
            $output['html'] .= '					<input type="submit" class="rounded-corners" id="add" name="add" value="ADD" />';
            $output['html'] .= '	   			 	<input type="hidden" name="product_id" id="product_id" value="'.$sale_id.'" />';
            $output['html'] .= '				</form>';
			$output['html'] .= '			</div>';
			$output['html'] .= '		</td>';
			$output['html'] .= '	</tr>';	
			$output['html'] .= '</table>';
			$output['html'] .= '</div>';

			$hash_url = ('#' . $aisle_id);
			$hash_url .= '/' . preg_replace('/[^a-z0-9]/i', '-', $aisle_name);
			$output['parent_hash_url'] = $hash_url;
			
			$odd_even = ($odd_even == "odd")? "even" : "odd";          
		}
	endif;
	echo json_encode($output);
	
	
?>