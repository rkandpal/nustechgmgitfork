<?php 
	require_once('_inc.php');
	require_once('functions.php');
	$head_title = array();
	$head_title[] = 'Search';
	$prod_id = $_GET['pid'];
?>
<?php require_once('_header.php'); ?>
<?php get_search(); ?>
<div id="mini-shopping-list" class="rounded-corners">
<?php include_once(dirname(__FILE__) . '/includes/mini_shopping_list.php') ?>
</div>
<?php require_once('_footer.php'); ?>